import React, { useState } from 'react';
import axios, { AxiosError } from 'axios';

interface LoginResponse {
  token: string;
}

const LoginForm: React.FC = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [message, setMessage] = useState('');
  const [errorMessage, setErrorMessage] = useState('');

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    setErrorMessage('');
    setMessage('');
    if (name === 'username') {
      setUsername(value);
    } else if (name === 'password') {
      setPassword(value);
    }
  };

  const handleSubmit = async (event: React.FormEvent) => {
    event.preventDefault();
  
    try {
      const response = await axios.post<LoginResponse>('/api/auth', {
        username,
        password,
      });
  
      const token = response.data.token;
      setMessage('Данные успешно отправлены на сервер!');
      setErrorMessage('');
    } catch (error: unknown) {
      console.error(error);
  
      if (axios.isAxiosError(error)) {
        const axiosError = error as AxiosError;
        if (axiosError.response && axiosError.response.status === 400) {
          setErrorMessage('Необходимо заполнить поля UserName и Password.');
        } else {
          setErrorMessage('Произошла ошибка при запросе к серверу!');
        }
      } else {
        setErrorMessage('Произошла неизвестная ошибка.');
      }
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <div>
        <h3>Запрос на сервер (если логин и пароль пустые - вернет ошибку)</h3>
        <label>
          Username:
          <input
            type="text"
            name="username"
            value={username}
            onChange={handleInputChange}
          />
        </label>
      </div>
      <div>
        <label>
          Password:
          <input
            type="password"
            name="password"
            value={password}
            onChange={handleInputChange}
          />
        </label>
      </div>
      {message && <p>{message}</p>}
      {errorMessage && <p>{errorMessage}</p>}
      <button type="submit">Log In</button>
    </form>
  );
};

export default LoginForm;