import React from 'react';
import LoginForm from './LoginForm';

const App: React.FC = () => {
  return (
    <div>
      <LoginForm />
    </div>
  );
};

export default App;
